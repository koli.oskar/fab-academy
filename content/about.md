---
title: About
description: 
---


My name is Oskar Koli, nice to meet you!


My background was originally technical, I started programming around the age of 14 to be able to make Flash web games.
Right after high school I got an internship at Rovio, the creators of well known mobile game Angry Birds.
After that I worked as a programmer for a few years, while on the side doing a bachelors in Computer Science.

But quite recently, at the age of 26, I realized that I wanted to expand my knowledge and go for more creative pursuits!
So I applied and got into a design program at Aalto University in Helsinki.
As of writing this, I've only been in the program for half a year and I've already learned such things as: physical computing, metal & wood work, CAD software, CNC machining, laser cutting, Pure Data and design research skills.

I decided to do Fab Academy to complement what I'd already learned and to deepen my knowledge.