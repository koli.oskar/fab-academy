---
title: Final Project
description: 
---
For my final project I want to create a tentacle robot which is attached to a base and is able to look around and interact/react to its' environment in a way that would make people humanize it.

![Robot Overview](/tentacle_overview.jpg)

## Prior Works & Improvements

I will base the design on [this project from HackADay](https://hackaday.com/2016/09/13/the-bootup-guide-to-homebrew-two-stage-tentacle-mechanisms/).

I will expand on the project by making the movement of the tentacle computer controller instead of manually controlled. Additionally I want to add a camera at the "head" of the tentacle which would allow it to detect faces and thus look at and interact with the audience. If I have the time, I would also like there to be a couple of directional microphones at the base of the robot which could be used to detect sudden/scary sounds around the robot.

## Construction

As in the HackADay project, the tentacle will be controlled by a pulley mechanism.
The tentacle would have two independent segments which both have two degrees of freedom in their movements.

The core of the tentacle should be a soft, but still resist twisting.
The segments of the tentacle will be made out of laser cut plastic.

![Segment Sideview](/tentacle_segment_side_view.jpg)

![Segment Isometric](/tentacle_segment_isometric.jpg)

![Segment Overview](/segment_overview.jpg)

## AI & Behavior

For the AI of the robot I will use a [finite state machine](https://en.wikipedia.org/wiki/Finite-state_machine). 
In other words, all the behavior the robot will have will be manually designed.

Here is a list of couple of example behaviors:

- If idle and a face is visible in the camera, look at the face.
- If you've been looking at a face for long, get bored and start idling.
- If a loud noise is detected, look in the direction of the sound.
- If idle and no one is close, dance around.


## Target Audience

I imagine the robot being on display in some public place.
The hope would be that passerby would notice it and get curious.
Once the passerby is close enough, the robot would "see" them through its' camera and react to their presence. 

The hope would be for people to be delighted by the behavior of the robot
and to on some level see it as a living thing!
In other words, the main motivation for the project is to create a character that seems to have a personality even though it is just a robot.

## Development plan

The high level plan for making the project is as follows:

1. Create a first version of the tentacle, similar to the HackADay one.
2. Make the tentacle motor actuated.
3. Add a camera to the end of the tentacle.
4. "Dress up" the robot, meaning that I would create some kind of a skin for it.

I'd be happy if I was able to complete at least steps 1 and 2 during Fab Academy.
If the movement of the tentacle is as good as I want it, then I will keep working on the project outside the course.



### Relevant links:

- [HackADay Tentacle](https://hackaday.com/2016/09/13/the-bootup-guide-to-homebrew-two-stage-tentacle-mechanisms/)
- [Jamming Robots](http://fab.cba.mit.edu/classes/865.18/motion/jamming/jamming-robots.pdf)