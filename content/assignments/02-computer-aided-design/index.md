---
title: "Week 02: Computer-Aided Design"
date: 2020-02-09
tags: []
draft: false
---

This week we were tasked with exploring different digital modeling tools and to model some part of the final project with them.

I decided to concentrate on modeling the segments of the tentacle.
I did the modeling both with OpenSCAD (which I hadn't used before) and Fusion 360 (which I'm quite familiar with). Additionally I tried to simulate the bending of the tentacle in Blender.

Here is a 2D overview of what the segment I wanted to model looks like:

![Segment Overview](../../segment_overview.jpg)

## OpenSCAD

[OpenSCAD](https://www.openscad.org) is a "Programmers Solid 3D CAD Modeller". In OpenSCAD all modeling is done by simple programming commands. 

I was able to model the segment without too much trouble, even though I had never used OpenSCAD before,
the [OpenSCAD cheatsheet](https://www.openscad.org/cheatsheet/) was all I needed to get things working!

The biggest problem I had was that there does not seem to be any simple way to draw the curves that I need for the wire conduit slots! To solve this problem I ended up drawing a bunch of circles along a path which were so close together that they created the curved slot when cut out of the main cylinder.

Here is the OpenSCAD code that I ended up with:
{{< highlight openscad>}}
coreRadius = 3.175 / 2;
segmentOuterRadius = 25;
segmentInnerSpace = 0.5;
segmentThickness = 4;
controlWireHoleRadius = 1.5;
controlWireHoleSpacing = 1;
controlWireHoleOffset = 2;
conduitRadius = 3.5;


difference() {
    // Draw the main cylinder
    linear_extrude(height = segmentThickness, center = true)
        circle(segmentOuterRadius, $fn=100);
    
    
    // Remove parts from the main cylinder
    linear_extrude(height = segmentThickness + 1, center = true) {
        // Core hole
        circle(coreRadius, $fn=50); 
        
        for (i =[0:3]) {
            // Control wire holes
            rotate([0, 0, i * 90])    
            translate([0, segmentOuterRadius - controlWireHoleRadius - controlWireHoleSpacing - controlWireHoleOffset,-1])
                circle(controlWireHoleRadius + controlWireHoleSpacing, $fn=50); 
            
            // Wire Conduit slots
            for (j =[(i * 90 + 25):((i + 1) * 90 - 25)]) {
                rotate([0, 0, j])    
                translate([0, 
                    segmentOuterRadius - controlWireHoleRadius 
                    - controlWireHoleSpacing - controlWireHoleOffset,-1])
                    circle(conduitRadius, $fn=50);
            }
        }
    }
}
{{< / highlight >}}

And here is an image of the model:

![OpenSCAD segment](./openscad_segment.png)

My general feeling of OpenSCAD is that I quite like the idea, because programming as an interface is so much more flexible and powerful than a GUI.
But at the same time, it is very clear that Fusion 360 has much more features. 
In fact, OpenSCAD seems quite limited in what it can do.

## Fusion 360

*Note: I ended up going a bit further and actually modeled a part of the tentacle instead of just the segment like I did in OpenSCAD.*

I started off by creating a sketch with a single circle, which I then extruded to 30 cm.
This is the core of the tentacle, which in the final build should be some type of flexible material that resists twisting.

![Created the core](./fusion_core.jpg)

Next I created a second sketch, projected the outline of the core into it and started designing the segment. The most difficult part of this step was figuring out how I would make the curved wire conduit slots. 

![Segment sketch](./segment_sketch.jpg)

I ended up solving it nicely by making some additional circles in the sketch, which created the curves together with the end holes of the conduit slots.

![Segment extrusion selection](./segment_extrusion_selection.jpg)

I then extruded the selection,

![Segment](./segment.jpg)

after which I used the rectangular pattern tool to duplicate the segment along the core of the tentacle:

![Segment pattern](./segment_pattern.jpg)

And here is the final model:

![Final](./final.jpg)

I also looked into if it would be possible to simulate the movement of the soft bendable core in Fusion 360, but concluded that it currently isn't possible.

P.S. Something that may not be clear from the pictures above, is that the Fusion 360 model is fully parametric and constrained.

## Blender

[Blender](https://www.blender.org/) is a 3D modeling and animation tool, aimed at animation, visual effects, art and motion graphics. 

I wanted to try and simulate the bending of the tentacle (which I modeled in Fusion 360) in Blender.

I exported the Fusion 360 model to an STL file, then I imported that file into Blender.
I then spent a couple of hours trying to get the soft body simulation working in a way that the inner core would behave like rubber, without much success. I was able to get the physics system working, but the core kept either compressing like an empty balloon with a hole in it or then it didn't bend at all.