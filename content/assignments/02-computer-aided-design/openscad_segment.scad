
coreRadius = 3.175 / 2;
segmentOuterRadius = 25;
segmentInnerSpace = 0.5;
segmentThickness = 4;
controlWireHoleRadius = 1.5;
controlWireHoleSpacing = 1;
controlWireHoleOffset = 2;
conduitRadius = 3.5;


difference() {
    // Draw the main cylinder
    linear_extrude(height = segmentThickness, center = true)
        circle(segmentOuterRadius, $fn=100);
    
    
    // Remove parts from the main cylinder
    linear_extrude(height = segmentThickness + 1, center = true) {
        // Core hole
        circle(coreRadius, $fn=50); 
        
        for (i =[0:3]) {
            // Control wire holes
            rotate([0, 0, i * 90])    
            translate([0, segmentOuterRadius - controlWireHoleRadius - controlWireHoleSpacing - controlWireHoleOffset,-1])
                circle(controlWireHoleRadius + controlWireHoleSpacing, $fn=50); 
            
            // Wire Conduit slots
            for (j =[(i * 90 + 25):((i + 1) * 90 - 25)]) {
                rotate([0, 0, j])    
                translate([0, 
                    segmentOuterRadius - controlWireHoleRadius 
                    - controlWireHoleSpacing - controlWireHoleOffset,-1])
                    circle(conduitRadius, $fn=50);
            }
        }
    }
}


