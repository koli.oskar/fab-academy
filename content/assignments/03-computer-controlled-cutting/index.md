---
title: "Week 03: Computer-Controlled Cutting"
date: 2020-02-19
tags: []
draft: false
---

This weeks theme was computer-controlled cutting.
The week's main task was to parametrically design and laser cut a press-fit kit.
The secondary task was a group task where we should figure out our laser cutters focus, power, speed, rate, kerf, and joint clearance.



# 1. Laser cutter

*NOTE: I used two different laser cutters during this week, one at the Fab Lab and one at the university. 
The kerf and joint clearance mentioned below are for the Fab Lab one.*

The laser cutter at our Fab Lab is an Epilog LEGEND 36EXT.

![Laser cutter](./laser_cutter.jpg)

### 1.1 Kerf

To figure out the kerf of the laser cutter, I cut out multiple 20x20mm pieces of 4mm plywood and then 
measured their true dimensions of the pieces and averaged the offset.
Doing this I figured out that the kerf of the machine is 0.125mm.

![Kerf test model](./kerf_test.jpg)

![Kerf test pieces](./klerf_test_pieces.jpg)

### 1.2 Joint clearance

To figure out the joint clearance of the laser, I used the joint which I designed for my press-fit kit (see the [Connectors section](#connectors) for more information).

![Joint clearance test pieces](./joint_clearance_test_pieces.jpg)

| Clearance / Offset  | Result  |
|---|---|
|  0.15mm | Attachable by hand. Very loose.  |
| 0.1mm   | Attachable by hand. Stays together but not permanent.  |
| 0.07mm  | Attachable by hand with difficulty. Stays together, hard to take apart.  |
| 0.01mm  | Attachable by hand with difficulty. Stays together, hard to take apart.  |

From this we can see that to make a permanent joint (which would need to be hammered), the offset would need to be slightly negative. At least when using 4mm plywood, it might be different with other materials.


# 2. Press-Fit Kit

### 2.1 Living Hinges

A living hinge is a solid piece of material that is able to flex and bend. 
Often this is achieved by cutting patterns into the material, which gives it this flexibility.

I decided to use a lattice hinge pattern for my design, and chose to generate them in OpenSCAD.

![Flex Test Model](./flex_test_model.jpg)

Here is the code:
{{< highlight openscad>}}
/**
    Generates a latice living hinge pattern.
    length = The length of the piece
    width = The width of the piece
    lw_size = The width of the cutouts
    lw_min_width = The min width of the material between the cutouts.
*/
module latice_living_hinge(length, width, lw_size, lw_min_width) {
    lw_width = width - lw_size;
    lw_cut_count = length / (lw_min_width + lw_size);

    difference() {
        square([length, width]);
        for (i = [1:lw_cut_count]) {
            x = i * (lw_min_width + lw_size) * 1;
            if (i % 2 == 0) {
                translate([x, -1, 0]) {
                    square([lw_size, ((width - lw_min_width) / 2) + 1]);
                }
                translate([x, ((width - lw_min_width) / 2) + lw_min_width, 0]) {
                    square([lw_size, ((width - lw_min_width) / 2) + 1]);
                }
            } else {
                translate([x, lw_min_width, 0]) {
                    square([lw_size, (width - lw_min_width * 2)]);
                }
            }
        }

    }
}
{{< / highlight >}}

By varying the lw_size and lw_min_width parameters if the module, I then generated the four different variations of the hinge.

Next I cut them from three different thicknesses of plywood: 0.6mm, 4mm and 8mm.

<video autoplay loop muted>
  <source src="./flex_piece_being_cut.mp4" type="video/mp4">
</video>

Pieces after laser cutting:

![Flex Test](./flex_piece_laser_cut.jpg)

All the pieces I cut:

![Flex Test Overview](./flex_test_overview.jpg)

The flex!

<video autoplay loop muted>
  <source src="./flex_test.mp4" type="video/mp4">
</video>

The result of the test was that all thicknesses flexed quite well for the smallest gap sizes, while the largest sizes flexed very little.
The 0.6mm plywood was also quite fragile and snapped easily at the thinner sizes.

Based on the test I concluded that I would use 4mm plywood at the second smallest gap size,
because this gave me good strong hinges while still being relatively thin (8mm plywood feels surprisingly thick!)


### 2.2 Connectors

Next I designed connectors which would allow the pieces in the press-fit kit to be connected.

![Connector Test Model](./connector_test_model.jpg)

{{< highlight openscad>}}
module connector_female(kerf, offset) {
    offset(r = -(kerf / 2 - offset / 2)) {
        square([y_size / 2, material_thickness], center=true);
        square([material_thickness, y_size / 2], center=true);
    }
}

module connector_male(kerf, offset, chamfer_size) {
     let(w = (y_size / 2 + kerf - offset), h=material_thickness * 1.5) {
        translate([material_thickness * 1.5 / 2, 0, 0]) {
            difference() {
                    polygon([
                        [-h / 4, -w / 2],
                        [-h / 2, -w / 2 - chamfer_size], 
                        [-h / 2, w / 2 + chamfer_size],
                        [-h / 4, w / 2],
                        [h / 2, w / 2],
                        [h / 2, -w / 2]
                    ]);
                    union() {
                        translate([h / 2, w / 2, 0]) {
                            rotate([0, 0, 45]) 
                                square(2, center = true);
                        }
                        translate([h / 2, -w / 2, 0]) {
                            rotate([0, 0, 45]) 
                                square(2, center = true);
                        }
                    }
            }
        }
    }
}

module connector_test_piece(kerf, offset, chamfer_size) {
    let(attachor_base_thickness = 5) {
        difference() {
            union() {
                difference() {
                    square(y_size, center=true);
                    connector_female(kerf, offset);
                }
             
                translate([y_size / 2 + attachor_base_thickness, 0, 0]) {
                    difference() {
                        union() {
                            square([attachor_base_thickness, y_size], center=true);
                            translate([attachor_base_thickness / 2, 0, 0]) {
                                connector_male(kerf, offset, chamfer_size);
                            }
                        }

                        translate([-attachor_base_thickness / 2 + 1, 0, 1])
                        color("red")
                        rotate([0, 0, -90]) 
                            text(str("cs = ", chamfer_size), size=2, valign="bottom", halign="center");
                    }
                }
            }
            
            translate([-y_size / 2 + 1, 0, 1])
            rotate([0, 0, -90]) 
                text(str("k=", kerf, ", o=", offset), size=2, valign="bottom", halign="center");
        }
    }
}
{{< / highlight >}}

The code allows for changing the kerf, offset (joint clearance) and the (male connector's) chamfer size.
I then cut a bunch of test pieces with varying offsets and chamfer sizes to figure out the perfect fit.

<video autoplay loop muted>
  <source src="./connector_cutting.mp4" type="video/mp4">
</video>

I also marked the pieces with text showing their kerf and offset to keep track of them.

![Connector Pieces](./connector_pieces.jpg)

After a bunch of testing I ended up choosing an offset of 0.1mm and chamfer size of 0.15mm.
These keep the joints together while at the same time allowing them to be separated easily by hand.


### 1.3 Final Design



![Laser cutter settings](./LaserSetup.jpg)