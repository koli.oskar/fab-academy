---
title: "Week 01: Principles and Practices, Project Management"
date: 2020-01-30
tags: []
draft: false
---

The task for week 1 was to create a documentation website.
This included creating a project under [Gitlab](https://gitlab.com), 
setup git and deploying the website.

1. [Git]({{< relref "#git" >}})
2. [Git Hosting]({{< relref "#git-hosting" >}})
3. [Hugo]({{< relref "#hugo" >}})
4. [Netlify]({{< relref "#netlify" >}})
5. [Custom Domain]({{< relref "#customdomain" >}})

## Git
Git is a version control software, which means that it allows you to keep a record of the changes you make to your files. This then makes it much safer to make changes, because you can always go back to an older version.


Git has notoriously many features, which most people will never use. 
But learning the basics is not difficult, and once you know the basics you can always dive deeper when you need too! 
For a how-to on git, there is really no better source than the Pro Git book, written by Scott Chacon and Ben Straub,that can be found on [the official git website](https://git-scm.com/book/en/v2).

Just go through the [installation instructions](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) and then the [Git Basics](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository). More than that is overkill if you're just starting out!

## Git hosting

If you've learned the basics of git, you will know the concept of a remote git repository.
Hosting a remote can be done manually, but now-a-days there are a number of free git hosting providers that you can use instead of getting your hands dirty!
The most popular once are [Github](https://github.com/), [GitLab](https://gitlab.com/) and [Bitbucket](https://bitbucket.com/).

For this project I decided to go with GitLab.
So I created a public project and named it `fab-academy`.

[You can find the project here.](https://gitlab.com/koli.oskar/fab-academy)

## Hugo

I decided to use the static website generator [Hugo](https://gohugo.io/) to create the website, 
because I have used it from previouse projects.
For the template, I chose the [minimal theme](https://github.com/calintat/minimal).

Setting up and using hugo is quite simple, you can find good instructions in the [official documentation](https://gohugo.io/getting-started/quick-start/).

## Netlify

One of the easiest ways to host a Hugo website for *free* is using [Netlify](https://www.netlify.com/).
After creating an account, you click on the "New site from Git" button and it takes less then a minute to set up the website by giving Netlify access to the GitLab repository.

Netlify is able to automatically detect that Hugo is being used, built the website and deploy it.

## Custom Domain

By default, Netlify will give you a randomly generated domain. Which is not very pretty.
To get a custom domain, you will need to buy a domain name from a *domain registrar*.
The one I've been using is [Namecheap](https://www.namecheap.com/), but there are many others!

You will then need to follow the instructions in the "Domain Settings" section of your Netlify website.
This will include editing the "CNAME" record, which you should do on the domain registrar's website from where you bought your domain. Be aware that when you've edited the CNAME record, it will take at least a few minutes (if not a few hours) for the record to be propagated around the world.

If you want to understand what all this CNAME business is about, then you'll have to look into what the Domain Name System (DNS) is. A good primer for that is [this video](https://www.youtube.com/watch?v=mpQZVYPuDGU).